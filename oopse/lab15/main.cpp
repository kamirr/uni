#include <iostream>
#include <fstream>
#include <string>
#include <array>
#include <algorithm>
#include <vector>

#include "ui.hpp"
#include "character.hpp"
#include "hero.hpp"
#include "item.hpp"

int main() {
  const std::array<std::string, 8> cmds = {"create", "list", "equip", "show", "load", "save", "history", "monsters"};
  std::vector<Character> characters;
  std::vector<std::string> battle_history;
  ItemDb items{"items"};

  std::string cmd;
  std::cout
    << "create - character creator\n"
    << "list - lists characters\n"
    << "equip - given character equips all items\n"
    << "show - shows character details\n"
    << "load - loads character\n"
    << "save - saves character\n"
    << "monsters - monster randomizer\n"
    << "history - last 10 battles\n"
    << "Ctrl+D to exit" << std::endl;

  while(std::cout << "$ ", std::cin >> cmd) {
    if(std::find(cmds.begin(), cmds.end(), cmd) == cmds.end()) {
      std::cout << "invalid command: `" << cmd << "`" << std::endl;
      continue;
    }

    if(cmd == "create") {
      const auto new_chr = ui_create();
      std::cout << "Created character:" << std::endl;
      new_chr.pretty_print();
      characters.push_back(new_chr);
    } else if(cmd == "list") {
      ui_list(characters);
    } else if(cmd == "equip") {
      ui_equip_all(characters);
    } else if(cmd == "show") {
      ui_show(characters);
    } else if(cmd == "load") {
      characters.push_back(ui_load(items));
    } else if(cmd == "save") {
      ui_save(characters);
    } else if(cmd == "monsters") {
      ui_monsters(characters, battle_history, items);
    } else if(cmd == "history") {
      for(const auto &entry: battle_history) {
        std::cout << entry << std::endl;
      }
    }
  }

  std::cout << std::endl;
  return 0;
}
