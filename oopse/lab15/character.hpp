#ifndef character_hpp
#define character_hpp

#include <string>
#include <ostream>
#include <unordered_map>

#include "item.hpp"

struct ItemEntry {
  Item item;
  int owned;
  int equipped;
};

class Mage;
class Warrior;
class Berserker;
class Thief;

class Character {
protected:
  std::string name;
  int strength;
  int dexterity;
  int endurance;
  int intelligence;
  int charisma;
  int experience;
  std::vector<ItemEntry> inventory;

  /* only derived classes should use the empty constructor */
  Character() { }

public:
  Character(std::string name, int strength, int dexterity, int endurance, int intelligence, int charisma, int exp) {
    this->name = name;
    this->strength = strength;
    this->dexterity = dexterity;
    this->endurance = endurance;
    this->intelligence = intelligence;
    this->charisma = charisma;
    this->experience = exp;
  }

  Character(std::istream &is, ItemDb items) {
    is >> name
      >> strength
      >> dexterity
      >> endurance
      >> intelligence
      >> charisma
      >> experience;

    std::string item_name;
    int owned;
    int equipped;
    while(is >> item_name >> owned >> equipped) {
      this->inventory.push_back(ItemEntry {
        items.get_by_name(item_name),
        owned,
        equipped
      });
    }
  }

  void add_item(const Item& item) {
    for(auto &ie: this->inventory) {
      if(ie.item.name == item.name) {
        ie.owned += 1;
        return;
      }
    }
    this->inventory.push_back(ItemEntry{item, 0, 0});
  }

  void equip_all() {
    for(auto &ie: this->inventory) {
      ie.equipped = ie.owned;
    }
  }

  void save(std::ostream& os) const {
    os << name << " "
      << strength << " "
      << dexterity << " "
      << endurance << " "
      << intelligence << " "
      << charisma << " "
      << experience << "\n";

    for(const auto &ie: this->inventory) {
      os << ie.item.name << " " << ie.owned << " " << ie.equipped << "\n";
    }
  }

  const std::string& get_name() const {
    return this->name;
  }

  int get_strength() const {
    auto result = this->strength;
    for(const auto& ie: this->inventory) {
      result += ie.item.bonus * ie.equipped;
    }

    return result;
  }

  int& get_exp() {
    return this->experience;
  }

  void pretty_print() const {
    std::cout << "Name:       『" << this->name << "』\n"
      << "Strength:     " << this->strength << "\n"
      << "Dexterity:    " << this->dexterity << "\n"
      << "Endurance:    " << this->endurance << "\n"
      << "Intelligence: " << this->intelligence << "\n"
      << "Charisma:     " << this->charisma << "\n"
      << "Experience:   " << this->experience << "\n";
    
    std::cout << "Inventory:\n";
    for(const auto &ie: this->inventory) {
      std::cout << "『" << ie.item.name << "』";
      std::cout << "owned: "<< ie.owned << "; equipped: " << ie.equipped << "; strength: " << ie.item.bonus << std::endl;
    }
  }

  friend class Mage;
  friend class Warrior;
  friend class Berserker;
  friend class Thief;
};

#endif
