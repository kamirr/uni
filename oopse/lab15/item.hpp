#ifndef item_hpp
#define item_hpp

#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <functional>
#include <limits>
#include <cstdlib>

struct Item {
  std::string name;
  int bonus;
};

class ItemDb {
  std::vector<Item> items;

public:
  ItemDb(std::string path) {
    std::ifstream file{path};
    std::string name;
    int bonus;

    while(file >> name >> bonus) {
      this->items.push_back(Item{name, bonus});
    }
  }

  Item get_random() {
    for(const auto &item: this->items) {
      const double likelihood = double{std::hash<std::string>{}(item.name)} / double{std::numeric_limits<std::size_t>::min()} / 10;
      const double r = double{rand() % 1000} / 1000;
      if(r < likelihood) {
        return item;
      }
    }
    return Item{"null", 0};
  }

  // recursive :)
  Item get_by_name(std::string name, int idx = 0) {
    if(idx >= this->items.size()) {
      return Item{"null", 0};
    } else {
      if(this->items[idx].name == name) {
        return this->items[idx];
      } else {
        return this->get_by_name(name, idx + 1);
      }
    }
  }
};

#endif
