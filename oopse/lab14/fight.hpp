#include <string>

template<class T1, class T2>
std::string fight(T1& char1, T2& char2) {
  if(char1.get_strength() > char2.get_strength()) {
    char1.get_exp() += char2.get_exp();
    if(char2.get_exp() > 0) {
      char2.get_exp() -= 1;
    }
    return char1.get_name() + " won and earned " + std::to_string(char2.get_exp()) + "exp";
  } else {
    char2.get_exp() += char1.get_exp();
    if(char1.get_exp() > 0) {
      char1.get_exp() -= 1;
    }
    return char2.get_name() + " won and earned " + std::to_string(char1.get_exp()) + "exp";
  }
}
